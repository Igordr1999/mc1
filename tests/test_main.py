from django import test
from django.contrib.auth.models import User
from rest_framework.test import APIRequestFactory
from rest_framework.test import APITestCase
from data.models import Category, Build, Floor, Position, Node
from data.viewsets import PositionListView

# todo: test allow GET requests with filter params to API
# todo: test not allow POST and etc. requests to API
# todo: fix code below
host = "http://81.163.28.7"


class GetPublicViewTest(APITestCase):
    def test_home(self):
        response = self.client.get(host + '/')
        self.assertEqual(response.status_code, 404)

    def test_api_home(self):
        response = self.client.get(host + '/api')
        self.assertEqual(response.status_code, 404)

    def test_position_api(self):
        response = self.client.get(host + '/api/positions', format='json')
        self.assertEqual(response.status_code, 200)

    def test_node_api(self):
        response = self.client.get(host + '/api/nodes')
        self.assertEqual(response.status_code, 200)

    def test_anonymous_cannot_see_contacts(self):
        response = self.client.get(host + '/api/positions', format='json')
        self.assertEqual(response.status_code, 200)

    """
    def test_authenticated_user_can_see_contacts(self):
        user = User.objects.create_user("Petya," "petya123@ya.ru", "pass12345")
        self.client.force_login(user=user)
        response = self.client.get(host + '/api/positions')
        self.assertEqual(response.status_code, 200)
    """


class PositionModelTest(test.TestCase):

    @classmethod
    def setUpTestData(cls):
        Category.objects.create(name='Кабинет', code='c')
        Build.objects.create(number=1)

        Floor.objects.create(number=1, map='http://ya.ru/images', build_id=1)
        Floor.objects.create(number=2, map='http://ya.ru/images', build_id=1)

        Position.objects.create(category_id=1, floor_id=1, number=5, x=100, y=150, title="Кабинет 107")
        Position.objects.create(category_id=1, floor_id=2, number=6, x=200, y=350, title="Кабинет 108")

    def test_coordinates(self):
        position1 = Position.objects.get(id=1)
        field_list = position1.coordinates()
        self.assertEquals(field_list, [100, 150])

    def test_position_name(self):
        position2 = Position.objects.get(id=2)
        field_str = position2.position_name()
        self.assertEquals(field_str, "1b2f6c")

