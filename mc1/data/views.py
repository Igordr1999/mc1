from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Node, Build, Floor, Position, ReceptionTaskType, ReceptionWindow, ReceptionTask
from .serializers import NodeSerializer, ReceptionTaskTypeSerializer, \
    ReceptionWindowSerializer, ReceptionTaskSerializer


@csrf_exempt
def node_detail(request, pk):
    try:
        snippet = Node.objects.get(pk=pk)
    except Node.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == "GET":
        serializer = NodeSerializer(snippet)
        return JsonResponse(serializer.data)


@csrf_exempt
def getRoute(request):
    # todo: add the possibility of transitions inside the building

    if not request.GET.get('start_point', '') or not request.GET.get('stop_point', ''):
        return JsonResponse({})

    a = {}
    try:
        start_point = Position.get_by_code(code=request.GET['start_point'])
        stop_point = Position.objects.get(title=request.GET['stop_point'])
    except Position.DoesNotExist:
        return JsonResponse(a)

    # check same build
    if start_point.floor.build.number == stop_point.floor.build.number:
        # check same floor
        if start_point.floor.number == stop_point.floor.number:
            a = Position.get_my_dict(allow_build=[start_point.floor.build.number],
                                     allow_floor=[start_point.floor.number])

        else:
            a = Position.get_my_dict(allow_build=[start_point.floor.build.number],
                                     allow_floor=[start_point.floor.number,
                                                  stop_point.floor.number])

    else:
        a = Position.get_my_dict(allow_build=[start_point.floor.build.number],
                                 allow_floor=[start_point.floor.number,
                                              1])
        b = Position.get_my_dict(allow_build=[stop_point.floor.build.number],
                                 allow_floor=[stop_point.floor.number,
                                              1])
        a.update(b)
    return JsonResponse(a)


@csrf_exempt
def getAllReceptionTaskType(request):
    tasks = ReceptionTaskType.objects.all()
    serializer = ReceptionTaskTypeSerializer(tasks, many=True)
    return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def createReceptionTask(request):
    code = request.GET.get('code', '')
    task = ReceptionTask.create_task(code=code)
    if task:
        serializer = ReceptionTaskSerializer(task)
        return JsonResponse(serializer.data)
    else:
        return JsonResponse({})


@csrf_exempt
def startReceptionTask(request):
    window_number = request.GET.get('window_number', '')
    task = ReceptionTask.start_task(window_number=window_number)
    if task:
        serializer = ReceptionTaskSerializer(task)
        return JsonResponse(serializer.data)
    else:
        return JsonResponse({})


@csrf_exempt
def stopReceptionTask(request):
    task_id = request.GET.get('task_id', '')
    task = ReceptionTask.stop_task(task_id=task_id)
    if task:
        serializer = ReceptionTaskSerializer(task)
        return JsonResponse(serializer.data)
    else:
        return JsonResponse({})


@csrf_exempt
def rejectReceptionTask(request):
    task_id = request.GET.get('task_id', '')
    task = ReceptionTask.reject_task(task_id=task_id)
    if task:
        serializer = ReceptionTaskSerializer(task)
        return JsonResponse(serializer.data)
    else:
        return JsonResponse({})
