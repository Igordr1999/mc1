from rest_framework import serializers
from .models import Category, Build, Floor, Position, Node, \
    ReceptionTaskType, ReceptionWindow, ReceptionTask


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('name', 'code')


class BuildSerializer(serializers.ModelSerializer):
    class Meta:
        model = Build
        fields = ('build_name', 'number')


class FloorSerializer(serializers.ModelSerializer):
    build = BuildSerializer()

    class Meta:
        model = Floor
        fields = ('floor_name', 'build', 'map', 'number')


class PositionSerializer(serializers.ModelSerializer):
    floor = FloorSerializer()
    category = CategorySerializer()

    class Meta:
        model = Position
        fields = ('position_name', 'coordinates', 'title', 'number', 'floor', 'category')


class NodeSerializer(serializers.ModelSerializer):
    start_point = PositionSerializer()
    stop_point = PositionSerializer()

    class Meta:
        model = Node
        fields = ('start_point', 'stop_point', 'weight')


class ReceptionTaskTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReceptionTaskType
        fields = ('name', 'code')


class ReceptionWindowSerializer(serializers.ModelSerializer):
    task_types = ReceptionTaskTypeSerializer(many=True)

    class Meta:
        model = ReceptionWindow
        fields = ('task_types', 'open')


class ReceptionTaskSerializer(serializers.ModelSerializer):
    task_type = ReceptionTaskTypeSerializer()
    window = ReceptionWindowSerializer()

    class Meta:
        model = ReceptionTask
        fields = ('id', 'name', 'number', 'task_type', 'window', 'in_progress', 'close',
                  'rejected', 'created', 'updated', 'in_progress_start', 'in_progress_stop')
