from django.urls import path

from . import viewsets
from . import views

urlpatterns = [
    path('api/positions/', viewsets.PositionListView.as_view()),
    path('api/nodes/', viewsets.NodeListView.as_view()),
    path("api/node/<int:pk>", views.node_detail),

    path("api/getRoute/", views.getRoute),
    path("api/createReceptionTask/", views.createReceptionTask),
    path("api/startReceptionTask/", views.startReceptionTask),
    path("api/stopReceptionTask/", views.stopReceptionTask),
    path("api/rejectReceptionTask/", views.rejectReceptionTask),
    path("api/getAllReceptionTaskType/", views.getAllReceptionTaskType),
]
