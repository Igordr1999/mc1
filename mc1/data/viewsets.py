from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics
from .serializers import BuildSerializer, PositionSerializer, NodeSerializer
from .models import Build, Position, Node


class PositionListView(generics.ListAPIView):
    queryset = Position.objects.all()
    serializer_class = PositionSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('title', 'number', 'floor__number', 'floor__build__number', 'category__code')
    ordering_fields = ('title', 'number', 'floor__number', 'floor__build__number', 'category__code')
    search_fields = ('title', 'number', 'floor__number', 'floor__build__number', 'category__code')


class NodeListView(generics.ListAPIView):
    queryset = Node.objects.all()
    serializer_class = NodeSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('start_point__title', 'start_point__number', 'start_point__floor__number',
                     'start_point__floor__build__number', 'start_point__category__code',
                     'weight')
    ordering_fields = ('weight',)
    search_fields = ('weight',)
