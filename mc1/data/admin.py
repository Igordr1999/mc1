from django.contrib import admin
from .models import Category, Build, Floor, Position, Node, ReceptionTaskType, ReceptionWindow, ReceptionTask


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'code']


@admin.register(Build)
class BuildAdmin(admin.ModelAdmin):
    list_display = ['number']


@admin.register(Floor)
class FloorAdmin(admin.ModelAdmin):
    list_display = ['number', 'build', 'map']


@admin.register(Position)
class PositionAdmin(admin.ModelAdmin):
    list_display = ['title', 'category', 'floor', 'number', 'x', 'y']
    list_filter = ['category']


@admin.register(Node)
class NodeAdmin(admin.ModelAdmin):
    list_display = ['start_point', 'stop_point', 'weight']


@admin.register(ReceptionTaskType)
class ReceptionTaskTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'code']


@admin.register(ReceptionWindow)
class ReceptionWindowAdmin(admin.ModelAdmin):
    list_display = ['number', 'open']


@admin.register(ReceptionTask)
class ReceptionTaskAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'number', 'task_type', 'window', 'in_progress', 'close', 'rejected',
                    'created', 'updated', 'in_progress_start', 'in_progress_stop']
