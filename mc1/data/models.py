from django.db import models
from imagekit.models import ProcessedImageField
import re
from django.utils import timezone


class Category(models.Model):
    name = models.CharField(max_length=64, null=True, blank=True)
    code = models.CharField(max_length=1, unique=True)

    def __str__(self):
        return self.code

    class Meta:
        ordering = ["name"]


class Build(models.Model):
    number = models.IntegerField()

    def __str__(self):
        return self.build_name()

    def build_name(self):
        return str("{}b".format(self.number))

    class Meta:
        ordering = ["number"]


class Floor(models.Model):
    number = models.IntegerField()
    build = models.ForeignKey(Build, on_delete=models.CASCADE)
    map = models.URLField(max_length=128, null=True, blank=True)

    def __str__(self):
        return self.floor_name()

    def floor_name(self):
        return str("{}{}f".format(self.build.build_name(), self.number))

    class Meta:
        ordering = ["number"]


class Position(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    floor = models.ForeignKey(Floor, on_delete=models.CASCADE)
    number = models.IntegerField()
    x = models.IntegerField()
    y = models.IntegerField()
    title = models.CharField(max_length=64, null=True, blank=True)

    def __str__(self):
        return self.position_name()

    def position_name(self):
        return str("{}{}{}".format(self.floor.floor_name(), self.number, self.category))

    def coordinates(self):
        return [self.x, self.y]

    @classmethod
    def get_by_code(cls, code):
        _, build_number, floor_number, position_number = re.split("[a-z]", code)
        # build_type, floor_type, position_type, _ = re.split("[0-9]+", code)

        return cls.objects.get(floor__build__number=build_number,
                               floor__number=floor_number,
                               number=position_number,
                               category__code="c")

    @classmethod
    def get_my_dict(cls, allow_build, allow_floor):
        a = {}
        b = {}
        c = {}
        d = {}
        w = {}
        for x in Build.objects.filter(number__in=allow_build):
            for y in Floor.objects.filter(build=x, number__in=allow_floor):
                for z in Position.objects.filter(floor=y):
                    # for position field
                    c[z.position_name()] = z.coordinates()

                    # for nodes field
                    for z1 in Node.objects.filter(start_point=z):
                        d[z1.stop_point.position_name()] = z1.weight

                    if not d == {}:
                        d['title'] = z.title
                        w[z.position_name()] = d
                    d = {}

                # save
                q = {"graph": {}, "positions": c, "nodes": w}
                b[y.floor_name()] = q
                c = {}
                w = {}
            a[x.build_name()] = b
            b = {}
        return a

    class Meta:
        ordering = ["number", "x", "y"]


class Node(models.Model):
    start_point = models.ForeignKey(Position, on_delete=models.CASCADE, related_name='start_point')
    stop_point = models.ForeignKey(Position, on_delete=models.CASCADE, related_name='stop_point')
    weight = models.IntegerField()

    def __str__(self):
        return str("{} -> {}".format(self.start_point, self.stop_point))

    class Meta:
        ordering = ["-weight"]


class ReceptionTaskType(models.Model):
    name = models.CharField(max_length=64, unique=True)
    code = models.CharField(max_length=1, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class ReceptionWindow(models.Model):
    number = models.IntegerField(unique=True)
    task_types = models.ManyToManyField(ReceptionTaskType)
    open = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.number)

    class Meta:
        ordering = ["number"]


class ReceptionTask(models.Model):
    number = models.IntegerField(default=1)
    task_type = models.ForeignKey(ReceptionTaskType, on_delete=models.CASCADE)
    window = models.ForeignKey(ReceptionWindow, on_delete=models.CASCADE, null=True, blank=True)

    in_progress = models.BooleanField(default=False)
    close = models.BooleanField(default=False)
    rejected = models.BooleanField(default=False)   # request close=True
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    in_progress_start = models.DateTimeField(null=True, blank=True)
    in_progress_stop = models.DateTimeField(null=True, blank=True)

    def name(self):
        return "{}{:03}".format(self.task_type.code, self.number)

    @classmethod
    def create_task(cls, code):
        today = timezone.now().date()
        count = ReceptionTask.objects.filter(task_type__code=code, created__day=today.day,
                                             created__month=today.month, created__year=today.year).count()
        if count >= 999 or count == 0:
            count = 1
        else:
            count += 1
        return ReceptionTask.objects.create(number=count,
                                            task_type=ReceptionTaskType.objects.get(code=code))

    @classmethod
    def start_task(cls, window_number):
        # check close prev task for this window
        if ReceptionTask.objects.filter(in_progress=True, window__number=window_number):
            return None

        allow_types = ReceptionWindow.objects.get(number=window_number).task_types.all()
        task = ReceptionTask.objects.filter(in_progress=False,
                                            close=False,
                                            task_type__in=allow_types).order_by('created').first()
        if task:
            task.window = ReceptionWindow.objects.get(number=window_number)
            task.in_progress = True
            task.in_progress_start = timezone.now()
            task.save()
        return task

    @classmethod
    def stop_task(cls, task_id):
        task = ReceptionTask.objects.get(id=task_id)
        task.in_progress = False
        task.close = True
        task.in_progress_stop = timezone.now()
        task.save()
        return task

    @classmethod
    def reject_task(cls, task_id):
        task = ReceptionTask.objects.get(id=task_id)
        task.in_progress = False
        task.close = True
        task.rejected = True
        task.save()
        return task

    def __str__(self):
        return self.name()

    class Meta:
        ordering = ["-created"]
