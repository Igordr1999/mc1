# mc1 App 
This is a [Docker][] setup for a web application based on Django.

- The [Django][] application is served by [Gunicorn][] (WSGI application).
- We use [NginX][] as reverse proxy and static files server. Static and media files are
  persistently stored in volumes.
- Multiple [Postgres][] databases can be used. Data are persistently stored in volumes.
- [Python][] dependencies are managed through [pipenv][], with `Pipfile` and `Pipfile.lock`.
- Support for multiple environment settings (variable `DJANGO_SETTINGS_MODULE` is passed
  to the `djangoapp` service).
- Tests are run using [tox][], [pytest][], and other tools such as [safety][], [bandit][], [isort][] and [prospector][].
- Continuous Integration is configured for [GitLab][] with `.gitlab-ci.yml`.
  CI follows a Build-Test-Release flow. **WARNING**: this part is not fully functional yet.

Also a [Makefile][] is available for convenience. You might need to use `sudo make`
instead of just `make` because `docker` and `docker-compose` commands often needs
admin privilege.

## Requirements
You need to install [Docker][] and [Docker-Compose][].

## Build
`docker-compose build` or `make build`.

## Migrate databases
`docker-compose run --rm djangoapp mc1/manage.py migrate` or `make migrate`.

## Collect static files
`docker-compose run --rm djangoapp mc1/manage.py collectstatic --no-input'` or `make collectstatic`.

## Run
`docker-compose up` or `make run`.

## Tests
- `make checksafety`
- `make checkstyle`
- `make test`
- `make coverage`

## License
Software licensed under the [ISC license](/LICENSE).


## API Guide

Site: http://81.163.28.7

Methods (GET):
http://81.163.28.7/api/positions/

http://81.163.28.7/api/nodes/

- Поиск "Кабинет 107" по названию http://81.163.28.7/api/positions/?title=Кабинет%20107
- Поиск помещения на втором этаже: http://81.163.28.7/api/positions/?floor__number=2
- Поиск помещения по его типу (например, c - кабинет или w - душ) http://81.163.28.7/api/positions/?category__code=w
- Поиск в здании 1 на 2 этаже 9 кабинета (можно сделать поиск по коду): 
http://81.163.28.7/api/positions/?floor__build__number=1&floor__number=2&number=3&number=9&category__code=c
- Поиск по любому полю http://81.163.28.7/api/positions/?search=Кабинет%20107
- Сортируем по полю. Например, по убыванию номера кабинета http://81.163.28.7/api/positions/?ordering=-title
- Выполняем смещение и ограничиваем выборку http://81.163.28.7/api/positions/?limit=1&offset=1

- Тоже самое можно выполнять и с node:
Доступные поля 'start_point__title', 'start_point__number', 'start_point__floor__number',
                     'start_point__floor__build__number', 'start_point__category__code',
                     'weight'
   
